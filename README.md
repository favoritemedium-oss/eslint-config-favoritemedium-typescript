# FavoriteMedium JavaScript/TypeScript Style Guide

_Super charged with `@typescript-eslint`_

There are two styles configurations:

- [Vanilla Js/Ts](#setup_vanilla_js_ts)
- [React (Jsx/Tsx)](#setup_react)

> _if you are using React configurations then there is no need to import base configurations._

## Installation & Usage Guide

Following procedures will guide you through setting up `eslint-config-favoritemedium-typescript` into your projects.

## 1. Vanilla Js/Ts setup

Assuming `eslint` and `typescript` are already installed as development dependencies.

```sh
npm i -D eslint-config-favoritemedium-typescript \
        eslint-plugin-import@^2.22.1 \
        @typescript-eslint/eslint-plugin@^4.3.0 \
        eslint \
        typescript
```

Within ESLint config file at project root add following configurations:

```javascript
module.exports = {
  extends: ['favoritemedium-typescript'],
};
```

in `package.json` file:

```javascript
{
  //...
  "scripts": {
    "lint": "eslint ./<source_directory>/**/*",
    "lint:fix": "eslint --fix ./<source_directory>/**/*"
  }
  //...
}
```

> \*Note: **lint:fix** is optional command; it will try to fix semicolons, spacing, quotes related formatting automatically.

## 2. React setup

There are two possible ways that `eslint-config-favoritemedium-typescript` plugin can added to project.

1. Adding to project created with `npx create-react-app <cool-app> --template typescript`
1. Manually configuring to existing project

### 2.1 Adding to new/existing project created with [create-react-app](https://create-react-app.dev/docs/getting-started/) v3.x and higher

Generate react application with following command mentioned [here](https://create-react-app.dev/docs/adding-typescript):

```
npx create-react-app my-app --template typescript
# or
yarn create react-app my-app --template typescript
```

install following dependency:

```bash
yarn add -D eslint-config-favoritemedium-typescript
# or
npm i -D eslint-config-favoritemedium-typescript
```

create `.eslintrc` file in project root directory

```javascript
// .eslintrc

module.exports = {
  extends: ['favoritemedium-typescript/config/react'],
};
```

create `.eslintignore` file in project root directory:

```javascript
// .eslintignore

node_modules
*.svg
*.css
*.scss
```

in `package.json` file add following script.

```javascript
{
  //...
  "scripts": {
    "lint": "eslint ./<source_directory>/**/*",
  }
  //...
}
```

### 2.2 Manual configurations

For setting up eslint for react project you need to install three additional dependencies to vanilla js/ts setup

```javascript
npm i -D eslint-config-favoritemedium-typescript \
        eslint-plugin-import@^2.22.1 \
        @typescript-eslint/eslint-plugin@^4.3.0 \
        eslint-plugin-jsx-a11y@^6.3.1 \
        eslint-plugin-react@^7.21.2 \
        eslint-plugin-react-hooks@^4.1.2 \
        eslint \
        typescript
```

create `.eslintrc` file in project root directory

```javascript
// .eslintrc

module.exports = {
  extends: ['favoritemedium-typescript/config/react'],
};
```

create `.eslintignore` file in project root directory:

```javascript
// .eslintignore

node_modules
*.svg
*.css
*.scss
```

in `package.json` file:

```javascript
{
  //...
  "scripts": {
    "lint": "eslint ./<source_directory>/**/*"
  }
  //...
}
```

After above setup completion you may add `npm run lint` command to your `pre-commit` setup like configured in `.huskyrc.js`
