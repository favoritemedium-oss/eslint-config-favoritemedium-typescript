module.exports = {
  rules: {
    camelcase: 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      { selector: ['variableLike'], format: ['camelCase'] },
      { selector: 'interface', format: ['PascalCase'], prefix: ['I'] },
    ],

    // this rule is overriden for (.ts, .tsx) files in index.js
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-member-accessibility': [
      'error',
      {
        accessibility: 'explicit',
        overrides: {
          constructors: 'no-public',
        },
      },
    ],
  },
};
