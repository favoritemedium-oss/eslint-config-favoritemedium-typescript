module.exports = {
  extends: ['../base/index', './rules/react', './rules/react-a11y', './rules/react-hooks'].map(
    require.resolve,
  ),
  settings: {
    // Append 'ts' and 'tsx' extensions to FavoriteMedium 'import/resolver' setting
    'import/resolver': {
      node: {
        extensions: ['.js', '.ts', 'jsx', 'tsx', '.json'],
      },
    },
  },
  rules: {},
};
